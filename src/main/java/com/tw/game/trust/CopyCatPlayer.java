package com.tw.game.trust;

import java.util.Observable;
import java.util.Observer;

public class CopyCatPlayer extends Player implements Observer {

    private Move storeNextMove = Move.COOPERATE;
    public CopyCatPlayer(IPlayerMove moveBehaviour) {
        super(moveBehaviour);
    }

    public Move move() {
        return getStoreNextMove();
    }

    public Move getStoreNextMove() {
        return storeNextMove;
    }


    public void setStoreNextMove(Move storeNextMove) {
        this.storeNextMove = storeNextMove;
    }

    @Override
    public void update(Observable o, Object arg) {
        Move[] moveArr = (Move[]) arg;
        storeNextMove = storeNextMove == moveArr[0] ? moveArr[1] : moveArr[0];
    }
}
