package com.tw.game.trust;


public class Machine {
    public Score score(Move player1Move, Move player2Move) {
        if (player1Move == Move.COOPERATE && player2Move == Move.CHEAT) {
            return new Score(-1, 3);
        }

        if (player1Move == Move.CHEAT && player2Move == Move.COOPERATE) {
            return new Score(3, -1);
        }

        if (player1Move == Move.CHEAT && player2Move == Move.CHEAT) {
            return new Score(0,0);
        }

        return new Score(2, 2);
    }
}
