package com.tw.game.trust;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class DetectiveBehavior implements Observer, IPlayerMove {
    private int roundNumber;
    private List<Move> predefinedMoves;
    private boolean flagToChangeBehavior;
    public DetectiveBehavior() {
        predefinedMoves = new ArrayList<>();
        predefinedMoves.add(Move.COOPERATE);
        predefinedMoves.add(Move.CHEAT);
        predefinedMoves.add(Move.COOPERATE);
        predefinedMoves.add(Move.COOPERATE);
        this.roundNumber = 0;
    }

    @Override
    public void update(Observable o, Object arg) {
        Move[] moveArr = (Move[]) arg;
        //flagToChangeBehavior = moveArr[0] ? moveArr[1] : moveArr[0];
    }

    @Override
    public Move move() {
        if(roundNumber < 4) {
            Move currentMove = this.predefinedMoves.get(roundNumber);
            roundNumber++;
            return currentMove;
        }
        else{

        }
        return Move.COOPERATE;
    }
}
