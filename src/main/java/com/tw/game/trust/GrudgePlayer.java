package com.tw.game.trust;

import java.util.Observable;
import java.util.Observer;

public class GrudgePlayer extends Player implements Observer {
    private Move storeNextMove = Move.COOPERATE;
    public GrudgePlayer(IPlayerMove moveBehaviour) {
        super(moveBehaviour);
    }
    public Move move() {
        return getStoreNextMove();
    }

    public Move getStoreNextMove() {
        return storeNextMove;
    }
    @Override
    public void update(Observable o, Object arg) {

        Move[] moveArr = (Move[]) arg;
        storeNextMove =  moveArr[0].equals(Move.CHEAT) || moveArr[1].equals(Move.CHEAT) ? Move.CHEAT : Move.COOPERATE;
    }
}
