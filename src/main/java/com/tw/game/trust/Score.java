package com.tw.game.trust;

public class Score {
    private final int player2Score;
    private final int player1Score;

    public Score(int player1Score, int player2Score) {
        this.player1Score = player1Score;
        this.player2Score = player2Score;
    }

    public int forPlayer1() {
        return this.player1Score;
    }

    public int forPlayer2() {
        return this.player2Score;
    }
}
