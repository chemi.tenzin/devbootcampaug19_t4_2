package com.tw.game.trust;

import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;

public class Player {
    private final IPlayerMove moveBehaviour;
    private Scanner scanner;
    private int score = 0;

    public Player(IPlayerMove moveBehaviour) {
        //this.scanner = scanner;
        this.moveBehaviour = moveBehaviour;
    }

    public Move move() {
        return this.moveBehaviour.move();
    }

    public void addScore(int scoreToAdd) {
        this.score += scoreToAdd;
    }

    public int score() {
        return this.score;
    }

}
