package com.tw.game.trust;

import java.io.PrintStream;
import java.util.Observable;
import java.util.Observer;

public class Game extends Observable {
    private final Player player1;
    private final Player player2;
    private final Machine machine;
    private final int noOfRounds;
    private PrintStream printer;
    private CopyCatPlayer copyCatPlayer;

    public Game(Player player1, Player player2, Machine machine, int noOfRounds, PrintStream printer) {
        this.player1 = player1;
        this.player2 = player2;
        this.machine = machine;
        this.noOfRounds = noOfRounds;
        this.printer = printer;
    }

    public void play() {
        for(int i = 1; i <= this.noOfRounds; i++) {
            Move[] moveArray= new Move[2];
            moveArray[0] = getPlayer1Move();
            moveArray[1] = getPlayer2Move();
            Score currScore = this.machine.score(moveArray[0], moveArray[1]);
            this.setChanged();

            this.notifyObservers(moveArray);
            this.player1.addScore(currScore.forPlayer1());
            this.player2.addScore(currScore.forPlayer2());
            this.printer.println("Player1: " + this.player1.score() + ", Player2: "+ this.player2.score());
        }
        this.printer.println("Final Score:: Player1: " + this.player1.score() + ", Player2: "+ this.player2.score());
    }

    public Move getPlayer1Move() {
         return this.player1.move();
    }
    public Move getPlayer2Move() {
        return this.player2.move();
    }




}
