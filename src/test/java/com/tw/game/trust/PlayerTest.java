package com.tw.game.trust;

import org.junit.Assert;
import org.junit.Test;

import java.io.PrintStream;
import java.util.Scanner;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class PlayerTest {

    @Test
    public void shouldReadZeroAsInputAndReturnCooperate() {
        Player player = new Player(() -> new Scanner("0").nextLine().equals("0") ? Move.COOPERATE : Move.CHEAT);
        assertEquals(Move.COOPERATE, player.move());
    }

   @Test
    public void shouldReadOneAsInputAndReturnCheat() {
       Player player = new Player(() -> new Scanner("1").nextLine().equals("0") ? Move.COOPERATE : Move.CHEAT);
       assertEquals(Move.CHEAT, player.move());
    }

    @Test
    public void shouldTreatAnyNonZeroInputAsCheat() {
        Player player = new Player(() -> new Scanner("2").nextLine().equals("0") ? Move.COOPERATE : Move.CHEAT);
        assertEquals(Move.CHEAT, player.move());
    }

    @Test
    public void shouldUpdateScore() {
        Player player = new Player(() -> new Scanner("1").nextLine().equals("0") ? Move.COOPERATE : Move.CHEAT);
        player.addScore(1);
        assertEquals(1, player.score());
    }

    @Test
    public void shouldAlwaysCooperate() {
        Player player = new Player(() -> Move.COOPERATE);
        assertEquals(Move.COOPERATE, player.move());
    }

   @Test
    public void shouldAlwaysCheat() {
        Player player = new Player(() -> Move.CHEAT);
        assertEquals(Move.CHEAT, player.move());
    }

    @Test
    public void shouldReturnCopyCatMove() {
        CopyCatPlayer copyCatPlayer = new CopyCatPlayer(() -> Move.COOPERATE);
        assertEquals(Move.COOPERATE, copyCatPlayer.move());
    }


}