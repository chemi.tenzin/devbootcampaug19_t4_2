package com.tw.game.trust;

import org.junit.Assert;
import org.junit.Test;

import java.io.PrintStream;
import java.util.Observer;
import java.util.Scanner;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

public class GameIntegrationTest {

    @Test
    public void shouldPlayOneRoundOfGame() {
        // Arrange
        int noOfRounds = 1;
        Player player1 = new Player(() -> new Scanner("0").nextLine().equals("0") ? Move.COOPERATE : Move.CHEAT);
        Player player2 = new Player(() -> new Scanner("1").nextLine().equals("0") ? Move.COOPERATE : Move.CHEAT);
        Machine machine = new Machine();
        //Score score = new Score.class);
        PrintStream printer = mock(PrintStream.class);


        Game game = new Game(player1, player2, machine, noOfRounds, printer);

        // Act
        game.play();


        // Assert

        Assert.assertEquals(-1, player1.score());
        Assert.assertEquals(3, player2.score());
    }
    @Test
    public void shouldReturnCopyCatNextMove() {
        Machine machine = new Machine();
        Score score = mock(Score.class);
        PrintStream printer = mock(PrintStream.class);
        Player player = new Player(() -> Move.CHEAT);
        CopyCatPlayer copyCatPlayer = new CopyCatPlayer(() -> Move.COOPERATE);
        Game game = new Game(player, copyCatPlayer,machine, 5, printer);
        game.addObserver(copyCatPlayer);
        game.play();
        Assert.assertEquals(player.score(),3);
        Assert.assertEquals(copyCatPlayer.score(),-1);

    }

    @Test
    public void shouldReturnRoundBetweenCopyCatAndGrudge(){
        //CopyCatPlayer copyCatPlayer = new CopyCatPlayer(() ->  Move.COOPERATE);
        CopyCatBehavior copyCatBehavior = new CopyCatBehavior();
        Player copyCatPlayer = new Player(copyCatBehavior);
        GrudgeBehavior grudgeBehavior = new GrudgeBehavior();
        Player grudgePlayer = new Player(grudgeBehavior);
        Machine machine = new Machine();
        Score score = mock(Score.class);
        PrintStream printer = mock(PrintStream.class);
        Game game = new Game(grudgePlayer, copyCatPlayer,machine, 5, printer);
        game.addObserver(copyCatBehavior);
        game.addObserver(grudgeBehavior);

        game.play();
        Assert.assertEquals(grudgePlayer.score(),10);
        Assert.assertEquals(copyCatPlayer.score(),10);

    }

    @Test
    public void shouldReturnRoundBetweenCopyCatAndDetective(){
        CopyCatBehavior copyCatBehavior = new CopyCatBehavior();
        Player copyCatPlayer = new Player(copyCatBehavior);
        DetectiveBehavior detectiveBehavior = new DetectiveBehavior();
        Player detectivePlayer = new Player(detectiveBehavior);
        Machine machine = new Machine();
        Score score = mock(Score.class);
        PrintStream printer = mock(PrintStream.class);
        Game game = new Game(detectivePlayer, copyCatPlayer,machine, 5, printer);
        game.addObserver(copyCatBehavior);
        game.addObserver(detectiveBehavior);

        game.play();
        Assert.assertEquals(detectivePlayer.score(),8);
        Assert.assertEquals(copyCatPlayer.score(),8);

    }
}
