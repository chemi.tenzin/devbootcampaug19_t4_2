package com.tw.game.trust;

import org.junit.Test;

import java.io.PrintStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;


public class GameTest {

    @Test
    public void shouldInstantiateGameWithPlayers_Machine_NumberOfRounds() {
        Player player1 = mock(Player.class);
        Player player2 = mock(Player.class);
        Machine machine = mock(Machine.class);
        int noOfRounds = 1;
        PrintStream printer = mock(PrintStream.class);
        Game game = new Game(player1, player2, machine, noOfRounds, printer);

        assertNotNull(game);
    }

    @Test
    public void shouldPlayOneRoundOfGame() {
        // Arrange
        int noOfRounds = 1;
        Player player1 = mock(Player.class);
        Player player2 = mock(Player.class);
        Machine machine = mock(Machine.class);
        Score score = mock(Score.class);
        PrintStream printer = mock(PrintStream.class);
        when(score.forPlayer1()).thenReturn(2);
        when(score.forPlayer2()).thenReturn(2);
        when(machine.score(Move.COOPERATE, Move.COOPERATE)).thenReturn(score);
        when(player2.move()).thenReturn(Move.COOPERATE);
        when(player1.move()).thenReturn(Move.COOPERATE);
        Game game = new Game(player1, player2, machine, noOfRounds, printer);

        // Act
        game.play();


        // Assert
        verify(player1).move();
        verify(player2).move();
        verify(machine).score(Move.COOPERATE, Move.COOPERATE);
    }

    @Test
    public void shouldPlayTwoRoundsOfGame() {
        // Arrange
        int noOfRounds = 2;
        Player player1 = mock(Player.class);
        Player player2 = mock(Player.class);
        Machine machine = mock(Machine.class);
        Score score = mock(Score.class);
        PrintStream printer = mock(PrintStream.class);
        when(score.forPlayer1()).thenReturn(2);
        when(score.forPlayer2()).thenReturn(2);
        when(machine.score(Move.COOPERATE, Move.COOPERATE)).thenReturn(score);
        when(player2.move()).thenReturn(Move.COOPERATE);
        when(player1.move()).thenReturn(Move.COOPERATE);
        Game game = new Game(player1, player2, machine, noOfRounds, printer);

        // Act
        game.play();


        // Assert
        verify(player1, times(2)).move();
        verify(player2, times(2)).move();
        verify(machine, times(2)).score(Move.COOPERATE, Move.COOPERATE);
    }

    @Test
    public  void shouldBeAbleRetrievePlayersScoreAfterFiveRounds() {
        // Arrange
        int noOfRounds = 5;
        Player player1 = mock(Player.class);
        Player player2 = mock(Player.class);
        Machine machine = mock(Machine.class);
        Score score = mock(Score.class);
        PrintStream printer = mock(PrintStream.class);
        when(score.forPlayer1()).thenReturn(2);
        when(score.forPlayer2()).thenReturn(2);
        when(machine.score(Move.COOPERATE, Move.COOPERATE)).thenReturn(score);
        when(player2.move()).thenReturn(Move.COOPERATE);
        when(player1.move()).thenReturn(Move.COOPERATE);

        Game game = new Game(player1, player2, machine, noOfRounds, printer);

        // Act
        game.play();


        // Assert
        verify(player1, times(noOfRounds)).move();
        verify(player2, times(noOfRounds)).move();
        verify(player1, times(noOfRounds)).addScore(2);
        verify(player2, times(noOfRounds)).addScore(2);
        verify(machine, times(noOfRounds)).score(Move.COOPERATE, Move.COOPERATE);
    }

    @Test
    public  void shouldBeAbleRetrievePlayersScoreAfterThreeDifferentRounds() {
        // Arrange
        int noOfRounds = 3;
        Player player1 = mock(Player.class);
        Player player2 = mock(Player.class);
        Machine machine = mock(Machine.class);
        Score score = mock(Score.class);
        PrintStream printer = mock(PrintStream.class);
        when(score.forPlayer1()).thenReturn(2).thenReturn(3).thenReturn(3);
        when(score.forPlayer2()).thenReturn(2).thenReturn(-1).thenReturn(-1);
        when(player1.move()).thenReturn(Move.COOPERATE).thenReturn(Move.CHEAT).thenReturn(Move.CHEAT);
        when(player2.move()).thenReturn(Move.COOPERATE).thenReturn(Move.COOPERATE).thenReturn(Move.COOPERATE);
        when(machine.score(Move.COOPERATE, Move.COOPERATE)).thenReturn(score);
        when(machine.score(Move.CHEAT, Move.COOPERATE)).thenReturn(score);
        when(machine.score(Move.CHEAT, Move.COOPERATE)).thenReturn(score);

        Game game = new Game(player1, player2, machine, noOfRounds, printer);

        // Act
        game.play();

        // Assert
        verify(player1, times(noOfRounds)).move();
        verify(player2, times(noOfRounds)).move();
        verify(player1).addScore(2);
        verify(player1, times(2)).addScore(3);
        verify(player2).addScore(2);
        verify(player2, times(2)).addScore(-1);
        verify(machine).score(Move.COOPERATE, Move.COOPERATE);
        verify(machine, times(2)).score(Move.CHEAT, Move.COOPERATE);
    }


    @Test
    public void shouldPrintScoresToConsole() {
        // Arrange
        int noOfRounds = 2;
        Player player1 = mock(Player.class);
        Player player2 = mock(Player.class);
        Machine machine = mock(Machine.class);
        Score score = mock(Score.class);
        PrintStream printer = mock(PrintStream.class);

        when(player1.score()).thenReturn(2).thenReturn(4);
        when(player2.score()).thenReturn(2).thenReturn(4);
        when(player1.move()).thenReturn(Move.COOPERATE);
        when(player2.move()).thenReturn(Move.COOPERATE);
        when(machine.score(Move.COOPERATE, Move.COOPERATE)).thenReturn(score);

        Game game = new Game(player1, player2, machine, noOfRounds, printer);

        // Act
        game.play();

        // Assert
        verify(printer).println("Player1: 2, Player2: 2");
        verify(printer).println("Player1: 4, Player2: 4");
        verify(printer).println("Final Score:: Player1: 4, Player2: 4");
    }

    @Test
    public void shouldReturnPlayersMove(){
        int noOfRounds = 1;
        Player player1 = mock(Player.class);
        Player player2 = mock(Player.class);
        Machine machine = mock(Machine.class);
        PrintStream printer = mock(PrintStream.class);
        when(player1.move()).thenReturn(Move.COOPERATE);
        when(player2.move()).thenReturn(Move.CHEAT);


        Game game = new Game(player1, player2, machine, noOfRounds, printer);

        // Act
        game.getPlayer1Move();
        game.getPlayer2Move();
        // Assert
        verify(player1, times(noOfRounds)).move();
        verify(player2, times(noOfRounds)).move();


    }

}
