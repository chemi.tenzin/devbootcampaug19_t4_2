package com.tw.game.trust;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MachineTest {
    @Test
    public void shouldReturnScoreForPlayer1CooperateMoveAndPlayer2CooperateMove() {
        Machine machine = new Machine();
        Score score = machine.score(Move.COOPERATE, Move.COOPERATE);
        assertEquals(2, score.forPlayer1());
        assertEquals(2, score.forPlayer2());
    }

    @Test
    public void shouldReturnScoreForPlayer1CooperateMoveAndPlayer2CheatMove() {
        Machine machine = new Machine();
        Score score = machine.score(Move.COOPERATE, Move.CHEAT);
        assertEquals(-1, score.forPlayer1());
        assertEquals(3, score.forPlayer2());
    }

    @Test
    public void shouldReturnScoreForPlayer2CooperateMoveAndPlayer1CheatMove() {
        Machine machine = new Machine();
        Score score = machine.score(Move.CHEAT, Move.COOPERATE);
        assertEquals(3, score.forPlayer1());
        assertEquals(-1, score.forPlayer2());
    }

    @Test
    public void shouldReturnScoreForPlayer2CheatMoveAndPlayer1CheatMove() {
        Machine machine = new Machine();
        Score score = machine.score(Move.CHEAT, Move.CHEAT);
        assertEquals(0, score.forPlayer1());
        assertEquals(0, score.forPlayer2());
    }
}
