package com.tw.game.trust;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ScoreTest {
    @Test
    public void shouldInstantiateWithPlayerScores() {
        Score score = new Score(2, 2);
        assertNotNull(score);
    }

    @Test
    public void shouldRetrievePlayersScores() {
        Score score = new Score(2, 2);
        assertEquals(2, score.forPlayer1());
        assertEquals(2, score.forPlayer2());
    }
}
